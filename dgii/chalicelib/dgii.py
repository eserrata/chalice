import requests
from bs4 import BeautifulSoup

DGII_NCF_SERVICE = "https://dgii.gov.do/app/webApps/ConsultasWeb2/ConsultasWeb/consultas/ncf.aspx"
DGII_CIUDADANOS_SERVICE = "https://dgii.gov.do/app/WebApps/ConsultasWeb2/ConsultasWeb/consultas/ciudadanos.aspx"
DGII_RNC_SERVICE = "https://dgii.gov.do/app/webApps/ConsultasWeb2/ConsultasWeb/consultas/rnc.aspx"
DGII_RNC_REGISTRADOS_SERVICE = "https://dgii.gov.do/app/WebApps/ConsultasWeb2/ConsultasWeb/consultas/ciudadanos.aspx"


def _call_request_post(endpoint, headers, payload):
    session = requests.Session()
    url_numRnd = session.post(endpoint, headers=headers, data=payload).content
    html_content = BeautifulSoup(url_numRnd, 'html.parser')
    content_form = html_content.find(id="upMainMaster")
    return content_form


def _parse_response(content_form, query_type):
    if query_type == "encf":
        dgii_information = "No encontrado"
        cphMain_pResultado = content_form.find(id="cphMain_PResultadoFE")
        if cphMain_pResultado:
            rncemisor = content_form.find(id="cphMain_lblrncemisor").getText()
            rnccomprador = content_form.find(id="cphMain_lblrnccomprador").getText()
            encf = content_form.find(id="cphMain_lblencf").getText()
            CodSeguridad = content_form.find(id="cphMain_lblCodSeguridad").getText()
            EstadoFe = content_form.find(id="cphMain_lblEstadoFe").getText()
            MontoTotal = content_form.find(id="cphMain_lblMontoTotal").getText()
            TotalItbis = content_form.find(id="cphMain_lblTotalItbis").getText()
            FechaEmision = content_form.find(id="cphMain_lblFechaEmision").getText()
            FechaFirma = content_form.find(id="cphMain_lblFechaFirma").getText()

            return {
                "is_valid": True,
                "data": {
                    "rncemisor": rncemisor,
                    "rnccomprador": rnccomprador,
                    "encf": encf,
                    "CodSeguridad": CodSeguridad,
                    "EstadoFe": EstadoFe,
                    "MontoTotal": MontoTotal,
                    "TotalItbis": TotalItbis,
                    "FechaEmision": FechaEmision,
                    "FechaFirma": FechaFirma,
                }
            }
    elif query_type == "ncf":
        dgii_information = content_form.find(id="cphMain_lblInformacion").getText()
        cphMain_pResultado = content_form.find(id="cphMain_pResultado")
        if cphMain_pResultado:
            rnc_cedula = content_form.find(id="cphMain_lblRncCedula").getText()
            nombre_razon_social = content_form.find(id="cphMain_lblRazonSocial").getText()
            tipo_comprobante = content_form.find(id="cphMain_lblTipoComprobante").getText()
            ncf = content_form.find(id="cphMain_lblNCF").getText()
            estado = content_form.find(id="cphMain_lblEstado").getText()
            valido_hasta = content_form.find(id="cphMain_lblVigencia").getText()
            return {
                "is_valid": True,
                "data": {
                    "rnc_cedula": rnc_cedula,
                    "nombre_razon_social": nombre_razon_social,
                    "tipo_comprobante": tipo_comprobante,
                    "ncf": ncf,
                    "estado": estado,
                    "valido_hasta": valido_hasta,
                    "dgii_information": dgii_information
                }
            }
    elif query_type == "rnc":
        dgii_information = content_form.find(id="cphMain_MensajeATCumplio").getText()
        cphMain_dvDatosContribuyentes = content_form.find(id="cphMain_dvDatosContribuyentes")

        if cphMain_dvDatosContribuyentes.find_all('tr'):
            trs = cphMain_dvDatosContribuyentes.find_all('tr')
            result = dict([(tr.find_all('td')[0].get_text(), tr.find_all('td')[1].get_text()) for tr in trs])
            return {
                "is_valid": True,
                "data": {
                    "rnc": result["Cédula/RNC"].strip(),
                    "name": result["Nombre/Razón Social"].strip(),
                    "commercial_name": result["Categoría"].strip(),
                    "category": result["Categoría"].strip(),
                    "payment_regime": result["Régimen de pagos"].strip(),
                    "status": result["Estado"].strip(),
                    "actividad_economica": result["Actividad Economica"].strip(),
                    "administracion_local": result["Administracion Local"].strip(),
                    "dgii_information": dgii_information.strip(),
                    "query_type": query_type
                }
            }
        else:
            dgii_information = "El RNC/Cédula consultado no se encuentra inscrito como Contribuyente."
    elif query_type == "ciudadanos":
        cphMain_dvResultadoCedula = content_form.find(id="cphMain_dvResultadoCedula")
        if cphMain_dvResultadoCedula.find_all('tr'):
            trs = cphMain_dvResultadoCedula.find_all('tr')
            result = dict([(tr.find_all('td')[0].get_text(), tr.find_all('td')[1].get_text()) for tr in trs])
            return {
                "is_valid": True,
                "data": {
                    "rnc": result["RNC o Cédula"].strip(),
                    "name": result["Nombre"].strip(),
                    "commercial_name": "",
                    "category": "",
                    "payment_regime": result["Tipo"].strip(),
                    "status": result["Estado"].strip(),
                    "actividad_economica": result["Marca"].strip(),
                    "administracion_local": "",
                    "dgii_information": "",
                    "query_type": query_type
                }
            }
        else:
            dgii_information = "El RNC/Cédula consultado no se encuentra inscrito como Contribuyente."

    return {"is_valid": False, "error": dgii_information}


def _get_headers():
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36'
    }
    return headers


def get_encf_payload(rnc, ncf, ncf_comprador, codigo):
    payload = 'ctl00%24smMain=ctl00%24upMainMaster%7Cctl00%24cphMain%24btnConsultar'
    payload += '&ctl00%24cphMain%24txtRNC=' + rnc
    payload += '&ctl00%24cphMain%24txtNCF=' + ncf
    payload += '&ctl00%24cphMain%24txtRncComprador=' + ncf_comprador
    payload += '&ctl00%24cphMain%24txtCodigoSeg=' + codigo
    payload += '&__VIEWSTATE=WzgsEHsK79tM5oCKl84rTbFCxPb40w%2BjVIW1rUgRTtTXKHbtLBZ%2FOPEtUDIoaTUihgM1GGm6gJrxzF%2BVgWd6PHZR2qNK%2Bgf4ZiJ9Scv%2B2RQY0%2B0xppPl7s7Ob86mdSqZjTpW%2BhXgEhww4LW8QSjUb8AnVCIYz8TKVTphgqxLoZUeHoWElDGOsloE0LGc0UDcnhJZKfxVJVkrMAZCRWvaX%2F9phHiyWFtc1Vf4UPgHwhGE%2FPn8ZEbb%2BIfp5jIdmZdG6lezEjzpyavKDyNXtP58rP4M478SCSTBvE9htoxVphXdyvEDYoVhbv1K1AurexjKwJ6PXuAgJxMCIgEQgdGA9eKRZzPKU9NGAQd6flpyeG5ZDrw4Su9dUnc8ikayosFt26uXe%2FmCyn4leE%2BzSIivMEA6JCksaLfNagJNQP8PF5WVq5gF1f6iXEXJgXja0ZtcN6FG%2BDpLgTMDjGChGCMXMbFyAfU9b4q1qDLUvbn%2F0Xi77YXvYGTJ3q62L8oCSm9AjjIlCw%3D%3D'
    payload += '&__VIEWSTATEGENERATOR=43758EFE'
    payload += '&__EVENTVALIDATION=CjkDi87Lichg7zM1KPTc2rpRe8lp%2B07ntsDHK6cSpCmVH8LIodPKfcOnb4oX1R9BLrLIXJMB1gx%2BQDzIbfDis1UO1EEl8SS99trrRWmvPFQm4qClbw1tV%2FLr%2BF5jwR4YEufFRUrtgKKDptdK1YfIJCjUbvv9lWxA30HtMzAmJX%2B3ZFAPwjFNg6Vjor%2FKka8oPA2uGjlI33e1r4BkwnYXXYpDvnalB7WP2g9FR9131euTe86N'
    payload += '&ctl00%24cphMain%24btnConsultar=Buscar'
    return payload


def get_ncf_payload(rnc, ncf):
    payload = 'ctl00%24smMain=ctl00%24upMainMaster%7Cctl00%24cphMain%24btnConsultar'
    payload += '&ctl00%24cphMain%24txtRNC=' + rnc
    payload += '&ctl00%24cphMain%24txtNCF=' + ncf
    payload += '&__VIEWSTATE=WzgsEHsK79tM5oCKl84rTbFCxPb40w%2BjVIW1rUgRTtTXKHbtLBZ%2FOPEtUDIoaTUihgM1GGm6gJrxzF%2BVgWd6PHZR2qNK%2Bgf4ZiJ9Scv%2B2RQY0%2B0xppPl7s7Ob86mdSqZjTpW%2BhXgEhww4LW8QSjUb8AnVCIYz8TKVTphgqxLoZUeHoWElDGOsloE0LGc0UDcnhJZKfxVJVkrMAZCRWvaX%2F9phHiyWFtc1Vf4UPgHwhGE%2FPn8ZEbb%2BIfp5jIdmZdG6lezEjzpyavKDyNXtP58rP4M478SCSTBvE9htoxVphXdyvEDYoVhbv1K1AurexjKwJ6PXuAgJxMCIgEQgdGA9eKRZzPKU9NGAQd6flpyeG5ZDrw4Su9dUnc8ikayosFt26uXe%2FmCyn4leE%2BzSIivMEA6JCksaLfNagJNQP8PF5WVq5gF1f6iXEXJgXja0ZtcN6FG%2BDpLgTMDjGChGCMXMbFyAfU9b4q1qDLUvbn%2F0Xi77YXvYGTJ3q62L8oCSm9AjjIlCw%3D%3D'
    payload += '&__VIEWSTATEGENERATOR=43758EFE'
    payload += '&__EVENTVALIDATION=CjkDi87Lichg7zM1KPTc2rpRe8lp%2B07ntsDHK6cSpCmVH8LIodPKfcOnb4oX1R9BLrLIXJMB1gx%2BQDzIbfDis1UO1EEl8SS99trrRWmvPFQm4qClbw1tV%2FLr%2BF5jwR4YEufFRUrtgKKDptdK1YfIJCjUbvv9lWxA30HtMzAmJX%2B3ZFAPwjFNg6Vjor%2FKka8oPA2uGjlI33e1r4BkwnYXXYpDvnalB7WP2g9FR9131euTe86N'
    payload += '&ctl00%24cphMain%24btnConsultar=Buscar'
    return payload


def get_rnc_payload(rnc):
    payload = 'ctl00%24smMain=ctl00%24cphMain%24upBusqueda%7Cctl00%24cphMain%24btnBuscarPorRNC'
    payload += '&ctl00%24cphMain%24txtRNCCedula=' + rnc
    payload += '&ctl00%24cphMain%24txtRazonSocial='
    payload += '&__VIEWSTATE=V%2B2T5giIyqO1AR9VdqDy%2BLrfo5NTg%2FPOQGEkjJzfWddVaN9dJWIXoyU89s8yu6I%2F%2FaFpPexo4lZ3gbh60GG2%2Fr0H%2Bnz7d%2FWf3xo8ygBB7SUyJ565fW5CRBpPdY%2Bmip75gKBpKeczVV9weeYCbFjWYJUz%2BX9qlHGxvuXvZUYUSzW22bKTvOMpV4gUgkmpmViZ308lhWNTIQiKcakNIoV%2F3jGr2hsxlFRJj%2Frv34yplCM%2F%2B64UGFU%2FLV2wsvpEKpZpCgb9UQhh2qaNfuRccoHoZJiFGwU5eBXo4DpzjLwWUhk2OteU5GtP%2F5rsUyYOqU1MGDOhbUsfAvQE2R4jOZTIgDO8DDthHrKlF%2FFwKWzuwjaMrxWOAEcKVA%2F0o8XCAI%2FDoaWLafBmun9pxcc55u3hd2yWWqgYhBf675Iao18lX24A7PKNJ4goZX5YGultAIBsrT%2BB4zHZ1iEuIYMf2e328SZuynmoL4Bc%2BIIPDAUddUK9CXW%2BzHbJhox3Uednr7MhBpAoj9SN3981erVZm5REzOP4L3nloOzwuotyErIlyZzAzooRsSKnh8skkval1L7T%2FkgoxuHZJTT5zfV1sK0PzOYaVnfjiD8X9YjQPdp%2Fw2Bsgza1IlvPt84JW46u7FudztDi3X1SwSgXreeV8ymLEO5%2FGXDpeF1cKikrHBvSD8PSnfq8liWJV3l%2FJ3Jk68TeEuGB8Bcc5fuU09gXU28SUh7xkUd1S5kdusHcrwCt6iNm3r4DajCOthGXz3k8UvEuK%2F4SrQSDrQeMfOXoXaFu19v8%2BzYafApeedu0vStuWILkarN4y1L0HsWaNpik9kQE1tTs6hIpAL6uqfB28iSEZqIE6W5UuoFj7iyDHOchhwlQc9c6IseqwKiMhsNdyQIS8rkXaEeGNvPn59UTr9vyG%2BJ%2B%2Ffbom74aOP66urN6tqZAmXo54vk9ZT9xq5DqLxacEhiDAFx3yGUERDvtOAo2SI84uzt9UyLL7yaS9I35XvvbsewQdOQ3NS%2BcbNu%2F9n%2Bs5YRVIyg2md3hrjOQ5c5EvpXnKpPeAHd8rWQt3%2FG0AibkJJdAj5YjOJZL1OvJViwQD8eAgFw%2BouYyg6%2FeaKaggMM%2BpeBuMCr2AodiwW3hd8ns8M5wU4LED52%2F%2BHvUEN96EIMqk8Iovu%2B8REdLDxmA%2FLtoStktIYZEmXdmPzarxhB0lHau'
    payload += '&__VIEWSTATEGENERATOR=4F4BAA71'
    payload += '&__EVENTVALIDATION=onWGHgRaubeJRsmlQBDKStb2k1gI2jbulGYcilkJBMol7Ar99tFQSNuB3UHLOPsYXScdxGjxFUZx97PmjccitooiV4gUSgk5CiZVFYo%2ByXaWqu%2FEjm8UbvWL9m%2Bdu2MZFdSRnmqsHAxkQwEqATvGXzp1NUJo1FWHCTHZkGGcY77xmj6jn%2BzjsTjJb16yW4R4ih6ADqPawG9YUQiK4ywQnNQ%2B5mx6TDWp2WJ4KHY5UgUSSeEzCbbczxll01r3UY3Iw7BQ9g%3D%3D'
    payload += '&ctl00%24cphMain%24btnBuscarPorRNC=BUSCAR'
    return payload

def get_registrados_payload(rnc):
    payload = 'ctl00%24smMain=ctl00%24cphMain%24upBusqueda%7Cctl00%24cphMain%24btnBuscarCedula'
    payload += '&ctl00%24cphMain%24txtCedula=' + rnc
    payload += '&__VIEWSTATE=dnbI6jAPMgl%2Bm9JKV3%2BwL9IjOc3V%2FJmQ4o2PNrXsjPrW9JZ3ZMr8qXablJf98%2FMZmx1W2FAuCxqf%2FQ1zmkZtXLUWwyCzHJ2B8p2AQFw2%2BanGBdcqgksi5jiwAwJrwIK2%2FZxUs5WEDfzuJYcA%2B1d2ZMXSkeNHzxFOqNgYWj6vwWXqM9mZzrSoU0mZOOEI%2BM23exNvudItJAANNwkjMBjSRiFHrYvPhc9lnVNi8CfeU6StoxQQAxajeWzaPN%2BVAd%2B2oHshobbMwcjiN%2B8h0RrTI1nMlauLaYGNx7pEXKMS8stHub5yiifPQozG7ib%2BTuTQa7x9OQj3LM%2B%2BApRW9IZjSi2%2FHV4A2rb1A6TVnbDrdp1IXzsWK6nj8Yu8AeWyeBbGVhN1RABmFXFpUkL4o9%2B2130ZchEzE9k23%2FuuIGy9XrQVqBFd%2Bb84il%2FelpA28soMMLOAhBr7C1tlgdn%2BCji8y1LUHJaz9iL8qgr7joDbRlPQId%2F2h4wn0PXXzoX0gT%2F9HMyqHBkmVzr%2F4m7%2BJv8J2iKL7XANwAKqsQrgtkSEGS1xi3Ep8aJdxP%2FnXoRNRV7tp2NRn7Q2J6rZyjd4Wfn53TAxzDe1v7Zy0S0xNDFit6fewvEXNxTHO7GlnwA5BObIe8ov2C%2F84HpCVWbiGb%2F%2F4sJxmDVRhr41WoezFLwKVk5z8MDBeZDs0anphtDeW5ZltAQh30MtxFdj9i010bj8J7o3laXHqR82nojfqy5b%2F5PoHRdTOoihiMNARPIzO19RO%2FtCg3%2BfcE5nV7%2FM%2BPrT%2B5f4UuM%3D'
    payload += '&__VIEWSTATEGENERATOR=C8A53969'
    payload += '&__EVENTVALIDATION=O5Oz%2BeN%2Bzu6WK3gigmZ%2FC7Is7OD6ZxLAFkdLRjVKCM%2BOb1DhubpMLJ8sWI3T7i8V%2B7dKpy9ziE8CFt414KoOqlzHBRzpvWDn2wmWMZJoTBmMwUzQIzYd3FLV3QIQYOMl1brwnUvB4DHWSyQVYvODmZ2hrePcPXXTsUJ99lQfIa1fmHqF'
    payload += '&ctl00%24cphMain%24btnBuscarCedula=Buscar'
    return payload


def request_encf_dgii(rnc, ncf, ncf_comprador, codigo, endpoint=DGII_NCF_SERVICE):
    response = _call_request_post(endpoint, _get_headers(), get_encf_payload(rnc, ncf, ncf_comprador, codigo))
    return _parse_response(response, "encf")


def request_ncf_dgii(rnc, ncf, endpoint=DGII_NCF_SERVICE):
    response = _call_request_post(endpoint, _get_headers(), get_ncf_payload(rnc, ncf))
    return _parse_response(response, "ncf")


def request_rnc_dgii(rnc, endpoint=DGII_RNC_SERVICE):
    response = _call_request_post(endpoint, _get_headers(), get_rnc_payload(rnc))
    res = _parse_response(response, "rnc")

    if not res.get("is_valid") and len(rnc) == 11:
        response = _call_request_post(DGII_RNC_REGISTRADOS_SERVICE, _get_headers(), get_registrados_payload(rnc))
        res = _parse_response(response, "ciudadanos")

    return res
